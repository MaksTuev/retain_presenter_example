package ru.surfstudio.daggerexample.ui.screen.splash;

import dagger.Component;
import ru.surfstudio.daggerexample.app.AppComponent;
import ru.surfstudio.daggerexample.ui.base.PerScreen;
import ru.surfstudio.daggerexample.ui.base.ScreenComponent;
import ru.surfstudio.daggerexample.ui.base.fragment.FragmentModule;

@PerScreen
@Component(dependencies = AppComponent.class, modules = FragmentModule.class)
public interface SplashComponent extends ScreenComponent<SplashFragmentView>{
}
