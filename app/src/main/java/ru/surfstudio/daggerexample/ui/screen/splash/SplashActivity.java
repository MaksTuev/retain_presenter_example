package ru.surfstudio.daggerexample.ui.screen.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import ru.surfstudio.daggerexample.R;
import ru.surfstudio.daggerexample.ui.base.activity.BaseActivity;
import ru.surfstudio.daggerexample.ui.screen.main.MainActivityView;

public class SplashActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        if (savedInstanceState == null) {
            addFragment(R.id.container, new SplashFragmentView(), "AAA");
            getFragmentManager().executePendingTransactions();
        }
        new Handler().postDelayed(()->startActivity(new Intent(this, MainActivityView.class)), 2000);
    }

    protected void addFragment(int containerViewId, Fragment fragment, String tag) {
        FragmentTransaction fragmentTransaction = this.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(containerViewId, fragment, tag);
        fragmentTransaction.commit();

    }

}
