package ru.surfstudio.daggerexample.ui.base.fragment;

import android.app.Activity;
import android.support.v4.app.Fragment;

import javax.inject.Provider;

import dagger.Module;
import dagger.Provides;
import ru.surfstudio.daggerexample.ui.base.PerScreen;
import ru.surfstudio.daggerexample.ui.base.RetainScreenStateFragment;
import ru.surfstudio.daggerexample.ui.base.activity.ActivityWrapper;
import ru.surfstudio.daggerexample.ui.base.dialog.DialogManager;
import ru.surfstudio.daggerexample.ui.base.dialog.FragmentDialogManager;

@Module
public class FragmentModule {
    private RetainScreenStateFragment retainScreenStateFragment;

    public FragmentModule(RetainScreenStateFragment retainScreenStateFragment) {
        this.retainScreenStateFragment = retainScreenStateFragment;
    }

    @Provides
    @PerScreen
    ActivityWrapper provideActivityWrapper(Provider<Activity> activityProvider){
        return new ActivityWrapper(activityProvider);
    }

    @Provides
    @PerScreen
    FragmentWrapper provideFragmentWrapper(Provider<Fragment> fragmentProvider){
        return new FragmentWrapper(fragmentProvider);
    }

    @Provides
    @PerScreen
    Fragment provideFragment(){
        return retainScreenStateFragment.getTargetFragment();
    }

    @Provides
    @PerScreen
    Activity provideActivity(){
        return retainScreenStateFragment.getActivity();
    }

    @Provides
    @PerScreen
    DialogManager provideDialogManager(FragmentDialogManager dialogManager){
        return dialogManager;
    }
}
