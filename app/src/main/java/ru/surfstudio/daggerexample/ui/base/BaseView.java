package ru.surfstudio.daggerexample.ui.base;

import ru.surfstudio.daggerexample.ui.base.presenter.BasePresenter;

public interface BaseView {
    BasePresenter getPresenter();
    void initPresenter();
    ScreenComponent createScreenComponent();
}
