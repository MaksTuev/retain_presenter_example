package ru.surfstudio.daggerexample.ui.base.presenter;

public class EventWrapper<T> {
    private T normalObj;
    private Throwable errorObj;

    public EventWrapper(T normalObj) {
        this.normalObj = normalObj;
    }

    public EventWrapper(Throwable errorObj) {
        this.errorObj = errorObj;
    }

    public boolean isError(){
        return errorObj != null;
    }

    public T getNormalObj() {
        return normalObj;
    }

    public Throwable getErrorObj() {
        return errorObj;
    }
}
