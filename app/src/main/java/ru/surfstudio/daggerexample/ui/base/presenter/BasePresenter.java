package ru.surfstudio.daggerexample.ui.base.presenter;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

/**
 * базовый класс для презентера
 * <p>
 * имеет методы, соответствующие жизненному циклу view
 * при пересоздании View, заново создается и презентер
 *
 * @param <V> тип View
 *            если V не интерфейс, то использование методов View, относящихся к андроид фреймворку запрещено
 */
public class BasePresenter<V> {

    private CompositeSubscription subscriptions = new CompositeSubscription();
    private EventPermissionEmitter eventPermissionEmitter = new EventPermissionEmitter();
    private V view;

    public void attachView(V view) {
        this.view = view;
    }

    protected V getView() {
        return view;
    }

    public void onLoad(boolean reload) {
    }

    public void onLoadFinished() {
        eventPermissionEmitter.setPermissionGranted(true);
    }

    public void detachView() {
        eventPermissionEmitter.setPermissionGranted(false);
        view = null;
    }

    protected boolean isSubscriptionInactive(Subscription subscription) {
        return subscription == null || subscription.isUnsubscribed();
    }

    protected <T> Subscription subscribe(Observable<T> observable, Subscriber<T> subscriber) {
        Subscription subscription = Observable.zip(
                observable
                        .map(obj -> new EventWrapper(obj))
                        .onErrorReturn(err->new EventWrapper(err))
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnNext(o -> requestEventPermission())
                        .onBackpressureDrop(),
                Observable.create(produceEventPermissionEmitter()),
                (obj, permission) -> obj)
                .flatMap(eventWrapper -> eventWrapper.isError()
                        ? Observable.error(eventWrapper.getErrorObj())
                        : Observable.just((T)eventWrapper.getNormalObj()))
                .subscribe(subscriber);
        subscriptions.add(subscription);
        return subscription;
    }

    protected EventPermissionEmitter produceEventPermissionEmitter() {
        return eventPermissionEmitter;
    }

    protected <T> Subscription subscribe(Observable<T> observable, final Action1<T> onNext, final Action1<Throwable> onError) {
        return subscribe(observable, new Subscriber<T>() {
            @Override
            public void onCompleted() {
                // do nothing
            }

            @Override
            public void onError(Throwable e) {
                onError.call(e);
            }

            @Override
            public void onNext(T t) {
                onNext.call(t);
            }
        });
    }

    public void onDestroy() {
        if(!subscriptions.isUnsubscribed()) {
            subscriptions.unsubscribe();
        }
    }

    private void requestEventPermission() {
        produceEventPermissionEmitter().requestPermission();
    }
}
