package ru.surfstudio.daggerexample.ui.base.activity;

import android.app.Activity;

import javax.inject.Provider;

/**
 *
 */
public class ActivityWrapper {
    private Provider<Activity> provider;

    public ActivityWrapper(Provider<Activity> activityProvider) {
        this.provider = activityProvider;
    }

    public Activity get(){
        return provider.get();
    }
}
