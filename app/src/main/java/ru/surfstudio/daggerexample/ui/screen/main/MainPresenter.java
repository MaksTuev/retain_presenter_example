package ru.surfstudio.daggerexample.ui.screen.main;

import java.util.List;

import javax.inject.Inject;

import ru.surfstudio.daggerexample.entity.Book;
import ru.surfstudio.daggerexample.module.player.BookAudioPlayer;
import ru.surfstudio.daggerexample.module.repository.BookRepository;
import ru.surfstudio.daggerexample.ui.base.PerScreen;
import ru.surfstudio.daggerexample.ui.base.presenter.BasePresenter;
import rx.Subscription;
import timber.log.Timber;


@PerScreen
public class MainPresenter extends BasePresenter<MainActivityView> {

    private BookAudioPlayer bookAudioPlayer;
    private BookRepository bookRepository;

    private List<Book> books;

    private Subscription loadBooksSubscription;
    private Subscription loadMoreBookSubscription;

    @Inject
    public MainPresenter(BookAudioPlayer bookAudioPlayer, BookRepository bookRepository) {
        this.bookAudioPlayer = bookAudioPlayer;
        this.bookRepository = bookRepository;
    }

    @Override
    public void onLoad(boolean reload) {
        super.onLoad(reload);
        loadBook();
    }

    //---------------------- виды запросов в такой архитектуре
    //
    //Запросы I типа самые простые в обслуживании и распространенные ~70-80%
    //Запросы II типа - отвечают за загрузку основных данных экрана, должны содержать логику
    //                  1) использования прежних данных
    //                  2) отказа от запроса, если такой же запрос выполняется
    //                  Запросы II типа обычно выполняются только во время OnLoad
    //Запросы III типа - должны содержать логику отказа от запроса, если такой же запрос выполняется


    /**
     * I
     * пример обычного запроса
     *
     */
    public void purchaseBook(String bookId){
        subscribe(bookRepository.getBooks(books.size()),
                this::onPurchaseBookSuccess,
                err -> {/* handle error*/});
    }

    /**
     * II
     * пример запроса в котором:
     * -если есть загруженные данные, отображаем их на UI
     * -если запрос еще не выполнен, ничего не делаем
     * -в любом другом случае запрашиваем книги с сервера
     */
    private void loadBook() {
        if (books != null) {
            onLoadBookSuccess(books);
        } else if (isSubscriptionInactive(loadBooksSubscription)) {
            loadBooksSubscription = subscribe(
                    bookRepository.getBooks(0),
                    this::onLoadBookSuccess,
                    this::onLoadBookError);
        }
    }

    /**
     * III
     * пример запроса в котором:
     * -если запрос еще не выполнен, ничего не делаем
     * -в любом другом случае запрашиваем книги с сервера
     */
    public void loadMoreBooks(){
        if(isSubscriptionInactive(loadMoreBookSubscription)) {
            loadMoreBookSubscription = subscribe(
                    bookRepository.getBooks(books.size()),
                    this::onLoadMoreBookSuccess,
                    this::onLoadBookError);
        }
    }
 // ---------------------- ---------------------------------------------

    private void onPurchaseBookSuccess(Object o) {
        //todo
    }

    private void onLoadMoreBookSuccess(List<Book> books) {
        this.books.addAll(books);
        //todo getView().showBooks(books);
    }

    private void onLoadBookSuccess(List<Book> books) {
        this.books = books;
        //todo getView().showBooks(books);
    }

    private void onLoadBookError(Throwable err) {
        Timber.e(err, "load books error");
    }

}
