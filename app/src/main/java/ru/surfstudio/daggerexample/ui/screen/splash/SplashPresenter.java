package ru.surfstudio.daggerexample.ui.screen.splash;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import ru.surfstudio.daggerexample.interactor.bookfull.BookFullInteractor;
import ru.surfstudio.daggerexample.ui.base.presenter.BasePresenter;
import ru.surfstudio.daggerexample.ui.base.PerScreen;
import ru.surfstudio.daggerexample.ui.base.dialog.DialogManager;
import rx.Observable;
import timber.log.Timber;

/**
 * presenter экрана сплеша
 */
@PerScreen
public class SplashPresenter extends BasePresenter<SplashFragmentView> {

    private DialogManager dialogManager;
    private BookFullInteractor bookFullInteractor;

    @Inject
    public SplashPresenter(DialogManager dialogManager, BookFullInteractor bookFullInteractor) {
        this.dialogManager = dialogManager;
        this.bookFullInteractor = bookFullInteractor;
    }

    @Override
    public void onLoad(boolean reload) {
        super.onLoad(reload);
        Timber.d("Splash onLoad");
        Observable.combineLatest(
                Observable
                        .just(1)
                        .doOnCompleted(() -> Timber.d("onComplete 1")),
                Observable
                        .timer(2, TimeUnit.SECONDS)
                        .flatMap(t -> Observable.just(2, 3))
                        .doOnCompleted(() -> Timber.d("onComplete 2")),
                (integer, integer2) -> 0)
                .doOnCompleted(() -> Timber.d("onComplete full"))
                .subscribe();

    }
}
