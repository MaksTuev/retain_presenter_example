package ru.surfstudio.daggerexample.ui.screen.multy;

import javax.inject.Inject;

import ru.surfstudio.daggerexample.interactor.bookfull.BookFullInteractor;
import ru.surfstudio.daggerexample.ui.base.PerScreen;
import ru.surfstudio.daggerexample.ui.base.dialog.DialogManager;
import ru.surfstudio.daggerexample.ui.base.presenter.BasePresenter;
import ru.surfstudio.daggerexample.ui.screen.splash.SplashFragmentView;
import timber.log.Timber;

/**
 * presenter экрана сплеша
 */
@PerScreen
public class SimplePresenter extends BasePresenter<SplashFragmentView> {

    private DialogManager dialogManager;
    private BookFullInteractor bookFullInteractor;

    @Inject
    public SimplePresenter(DialogManager dialogManager, BookFullInteractor bookFullInteractor) {
        this.dialogManager = dialogManager;
        this.bookFullInteractor = bookFullInteractor;
    }

    @Override
    public void onLoad(boolean reload) {
        super.onLoad(reload);


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Timber.d("AAA simple presenter onDestroy");
    }
}
