package ru.surfstudio.daggerexample.ui.screen.splash;

import javax.inject.Inject;

import ru.surfstudio.daggerexample.ui.base.presenter.BasePresenter;
import ru.surfstudio.daggerexample.ui.base.ScreenComponent;
import ru.surfstudio.daggerexample.ui.base.fragment.BaseFragmentView;


/**
 * view экрана сплеша
 */
public class SplashFragmentView extends BaseFragmentView {

    @Inject
    SplashPresenter presenter;

    @Override
    public ScreenComponent createScreenComponent() {
        return DaggerSplashComponent.builder()
                .appComponent(getApplicationComponent())
                .fragmentModule(createFragmentModule())
                .build();
    }

    @Override
    public BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public String getName() {
        return "Splash";
    }
}
