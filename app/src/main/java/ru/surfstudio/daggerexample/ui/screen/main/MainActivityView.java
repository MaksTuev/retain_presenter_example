package ru.surfstudio.daggerexample.ui.screen.main;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import javax.inject.Inject;

import ru.surfstudio.daggerexample.R;
import ru.surfstudio.daggerexample.ui.base.ScreenComponent;
import ru.surfstudio.daggerexample.ui.base.activity.BaseActivityView;
import ru.surfstudio.daggerexample.ui.base.presenter.BasePresenter;
import ru.surfstudio.daggerexample.ui.screen.multy.MultiFragmentActivity;


public class MainActivityView extends BaseActivityView {

    @Inject
    MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new Handler().postDelayed(()->startActivity(new Intent(this, MultiFragmentActivity.class)), 4000);
    }

    @Override
    public ScreenComponent createScreenComponent() {
        return DaggerMainComponent.builder()
                .appComponent(getApplicationComponent())
                .activityModule(createActivityModule())
                .build();

    }

    @Override
    protected int getContentView() {
        return R.layout.main_activity;
    }

    @Override
    public String getName() {
        return "Main";
    }

    @Override
    public BasePresenter getPresenter() {
        return presenter;
    }
}
