package ru.surfstudio.daggerexample.ui.screen.main;

import dagger.Component;
import ru.surfstudio.daggerexample.app.AppComponent;
import ru.surfstudio.daggerexample.ui.base.PerScreen;
import ru.surfstudio.daggerexample.ui.base.ScreenComponent;
import ru.surfstudio.daggerexample.ui.base.activity.ActivityModule;

@Component(dependencies = AppComponent.class, modules = ActivityModule.class)
@PerScreen
public interface MainComponent extends ScreenComponent<MainActivityView> {
}
