package ru.surfstudio.daggerexample.ui.screen.multy;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import ru.surfstudio.daggerexample.R;
import ru.surfstudio.daggerexample.ui.base.activity.BaseActivity;

public class MultiFragmentActivity extends BaseActivity {

    public static final String FRAGMENT_1_ID = "FRAGMENT1_ID";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_fragments);
        if(getSupportFragmentManager().findFragmentByTag(FRAGMENT_1_ID)!=null) {
            FragmentTransaction fragmentTransaction = this.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(SimpleFragmentView.create(FRAGMENT_1_ID), FRAGMENT_1_ID);
            fragmentTransaction.commit();
        }

        View toogleBtn = findViewById(R.id.toggle_fragments);
        toogleBtn.setOnClickListener(v -> {
            toggleFragments();
        });
    }

    private void toggleFragments() {
        Fragment f = getSupportFragmentManager().findFragmentByTag(FRAGMENT_1_ID);
        if (f == null) {
            FragmentTransaction fragmentTransaction = this.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(SimpleFragmentView.create(FRAGMENT_1_ID), FRAGMENT_1_ID);
            fragmentTransaction.commit();
        } else {
            FragmentTransaction fragmentTransaction = this.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.remove(f);
            fragmentTransaction.commit();
        }
    }

   /* protected void addFragment(int containerViewId, Fragment fragment, String tag) {
        FragmentTransaction fragmentTransaction = this.getFragmentManager().beginTransaction();
        fragmentTransaction.add(containerViewId, fragment, tag);
        fragmentTransaction.commit();

    }*/
}
