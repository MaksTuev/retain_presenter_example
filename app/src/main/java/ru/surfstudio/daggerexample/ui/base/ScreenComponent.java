package ru.surfstudio.daggerexample.ui.base;

/**
 *
 */
public interface ScreenComponent<V> {
    void inject(V view);
}
