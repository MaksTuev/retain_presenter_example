package ru.surfstudio.daggerexample.ui.base.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;

import ru.surfstudio.daggerexample.ui.base.BaseView;
import ru.surfstudio.daggerexample.ui.base.HasName;
import ru.surfstudio.daggerexample.ui.base.RetainScreenStateFragment;
import ru.surfstudio.daggerexample.ui.base.ScreenComponent;
import ru.surfstudio.daggerexample.util.log.LogServerUtil;

/**
 * базовый класс для вью, основанной на Activity
 */
public abstract class BaseActivityView extends BaseActivity implements BaseView, HasName {

    private Handler handler = new Handler();
    private RetainScreenStateFragment retainStateFragment;

    @LayoutRes
    protected abstract int getContentView();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogServerUtil.logViewCreated(this);
        boolean stateFragmentCreated = initializeRetainScreenStateFragment();
        setContentView(getContentView());
        satisfyDependencies();
        initPresenter();
        boolean screenReload = !stateFragmentCreated;
        handler.post(() -> getPresenter().onLoad(screenReload));
        handler.post(() -> getPresenter().onLoadFinished());
    }


    /**
     *
     * @return создан ли фрагмент
     */
    protected boolean initializeRetainScreenStateFragment() {
        retainStateFragment = getRetainScreenStateFragment();
        boolean createdNewFragment = false;
        if(retainStateFragment == null){
            retainStateFragment = new RetainScreenStateFragment();
            FragmentTransaction fragmentTransaction = this.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(retainStateFragment, getName());
            fragmentTransaction.commit();
            this.getFragmentManager().executePendingTransactions();
            createdNewFragment = true;
        }
        return createdNewFragment;
    }

    protected ActivityModule createActivityModule(){
        if(retainStateFragment != null){
            return new ActivityModule(retainStateFragment);
        } else {
            throw new IllegalStateException("RetainScreenStateFragment must be created");
        }
    }

    @Nullable
    protected RetainScreenStateFragment getRetainScreenStateFragment() {
        return (RetainScreenStateFragment)getSupportFragmentManager().findFragmentByTag(getName());
    }

    /**
     */
    protected void satisfyDependencies(){
        ScreenComponent component = getScreenComponent();
        if(component == null){
            component = createScreenComponent();
            if(retainStateFragment != null){
                retainStateFragment.setScreenComponent(component);
            } else {
                throw new IllegalStateException("RetainScreenStateFragment must be created");
            }
        }
        component.inject(this);
    }

    @Nullable
    private ScreenComponent getScreenComponent(){
        return retainStateFragment == null
                ? null
                : retainStateFragment.getScreenComponent();
    }

    @Override
    @CallSuper
    public void initPresenter() {
        getPresenter().attachView(this);
        retainStateFragment.setOnDestroyListener(getPresenter()::onDestroy);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getPresenter().detachView();
        LogServerUtil.logViewDestroyed(this);
    }

}
