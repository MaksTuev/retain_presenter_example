package ru.surfstudio.daggerexample.ui.base.dialog;

import javax.inject.Inject;

import ru.surfstudio.daggerexample.ui.base.PerScreen;
import ru.surfstudio.daggerexample.ui.base.fragment.FragmentWrapper;

/**
 * отвечает за показывание и скрывание диалогов из презентера, когда вью является Fragment
 */
@PerScreen
public class FragmentDialogManager implements DialogManager {

    private FragmentWrapper fragmentWrapper;

    @Inject
    public FragmentDialogManager(FragmentWrapper fragmentWrapper) {
        this.fragmentWrapper = fragmentWrapper;
    }

    @Override
    public void show(BaseDialog dialog) {
        dialog.show(fragmentWrapper.get());
    }
}
