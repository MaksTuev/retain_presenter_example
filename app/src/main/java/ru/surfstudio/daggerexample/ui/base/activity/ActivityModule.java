package ru.surfstudio.daggerexample.ui.base.activity;

import android.app.Activity;

import javax.inject.Provider;

import dagger.Module;
import dagger.Provides;
import ru.surfstudio.daggerexample.ui.base.PerScreen;
import ru.surfstudio.daggerexample.ui.base.RetainScreenStateFragment;
import ru.surfstudio.daggerexample.ui.base.dialog.ActivityDialogManager;
import ru.surfstudio.daggerexample.ui.base.dialog.DialogManager;

@Module
public class ActivityModule {
    private RetainScreenStateFragment retainScreenStateFragment;

    public ActivityModule(RetainScreenStateFragment retainScreenStateFragment) {
        this.retainScreenStateFragment = retainScreenStateFragment;
    }

    @Provides
    @PerScreen
    Activity provideActivity(){
        return retainScreenStateFragment.getActivity();
    }

    @Provides
    @PerScreen
    ActivityWrapper provideActivityWrapper(Provider<Activity> activityProvider){
        return new ActivityWrapper(activityProvider);
    }

    @Provides
    @PerScreen
    DialogManager provideDialogManager(ActivityDialogManager dialogManager){
        return dialogManager;
    }

}
