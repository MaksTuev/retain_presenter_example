package ru.surfstudio.daggerexample.ui.screen.multy;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import javax.inject.Inject;

import ru.surfstudio.daggerexample.ui.base.ScreenComponent;
import ru.surfstudio.daggerexample.ui.base.fragment.BaseFragmentView;
import ru.surfstudio.daggerexample.ui.base.presenter.BasePresenter;
import timber.log.Timber;

public class SimpleFragmentView extends BaseFragmentView {

    public static final String EXTRA_ID = "EXTRA_ID";
    @Inject
    SimplePresenter presenter;

    @Override
    public BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public ScreenComponent createScreenComponent() {
        return DaggerSimpleComponent.builder()
                .appComponent(getApplicationComponent())
                .fragmentModule(createFragmentModule())
                .build();
    }

    public String getFragmentId(){
        return getArguments().getString(EXTRA_ID);
    }

    @Override
    public String getName() {
        return "SimpleFragmentView" + getArguments().getString(EXTRA_ID);
    }

    public static Fragment create(String id){
        Bundle b = new Bundle();
        b.putString(EXTRA_ID, id);
        Fragment f = new SimpleFragmentView();
        f.setArguments(b);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Timber.d("AAA simple fragment onCreate");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Timber.d("AAA simple fragment onDestroy");
    }
}
