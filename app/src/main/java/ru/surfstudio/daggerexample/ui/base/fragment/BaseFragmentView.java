package ru.surfstudio.daggerexample.ui.base.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import ru.surfstudio.daggerexample.app.App;
import ru.surfstudio.daggerexample.app.AppComponent;
import ru.surfstudio.daggerexample.ui.base.BaseView;
import ru.surfstudio.daggerexample.ui.base.HasName;
import ru.surfstudio.daggerexample.ui.base.RetainScreenStateFragment;
import ru.surfstudio.daggerexample.ui.base.ScreenComponent;
import ru.surfstudio.daggerexample.ui.base.activity.BaseActivity;
import ru.surfstudio.daggerexample.util.log.LogServerUtil;
import timber.log.Timber;


/**
 * базовый класс для вью, основанной на Fragment
 */
public abstract class BaseFragmentView extends Fragment implements BaseView, HasName {

    private Handler handler = new Handler();
    private RetainScreenStateFragment retainStateFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogServerUtil.logViewCreated(this);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Timber.d("OnActivity created");
        boolean stateFragmentCreated = initializeRetainScreenStateFragment();
        satisfyDependencies();
        initPresenter();
        boolean screenReload = !stateFragmentCreated;
        handler.post(() -> getPresenter().onLoad(screenReload));
        handler.post(() -> getPresenter().onLoadFinished());
    }

    /**
     *
     * @return создан ли новый фрагмент
     */
    protected boolean initializeRetainScreenStateFragment() {
        retainStateFragment = getRetainScreenStateFragment();
        boolean createdNewFragment = false;
        if(retainStateFragment == null){
            retainStateFragment = new RetainScreenStateFragment();
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.add(retainStateFragment, getName());
            fragmentTransaction.commit();
            this.getActivity().getFragmentManager().executePendingTransactions();
            createdNewFragment = true;
        }
        return createdNewFragment;
    }

    protected FragmentModule createFragmentModule(){
        if(retainStateFragment != null){
            return new FragmentModule(retainStateFragment);
        } else {
            throw new IllegalStateException("RetainScreenStateFragment must be created");
        }
    }

    @Nullable
    protected RetainScreenStateFragment getRetainScreenStateFragment() {
        return (RetainScreenStateFragment)getFragmentManager().findFragmentByTag(getName());
    }

    /**
     */
    protected void satisfyDependencies(){
        ScreenComponent component = getScreenComponent();
        if(component == null){
            component = createScreenComponent();
            if(retainStateFragment != null){
                retainStateFragment.setScreenComponent(component);
            } else {
                throw new IllegalStateException("RetainScreenStateFragment must be created");
            }
        }
        component.inject(this);
    }

    @Nullable
    private ScreenComponent getScreenComponent(){
        return retainStateFragment == null
                ? null
                : retainStateFragment.getScreenComponent();
    }

    @Override
    @CallSuper
    public void initPresenter() {
        getPresenter().attachView(this);
        retainStateFragment.setOnDestroyListener(getPresenter()::onDestroy);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getPresenter().detachView();
        LogServerUtil.logViewDestroyed(this);
    }

    protected AppComponent getApplicationComponent() {
        return ((App)getActivity().getApplication()).getAppComponent();
    }

    public BaseActivity getBaseActivity(){
        return (BaseActivity)getActivity();
    }
}
