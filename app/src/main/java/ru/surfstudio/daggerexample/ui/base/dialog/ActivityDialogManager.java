package ru.surfstudio.daggerexample.ui.base.dialog;

import android.support.v7.app.AppCompatActivity;

import javax.inject.Inject;

import ru.surfstudio.daggerexample.ui.base.PerScreen;
import ru.surfstudio.daggerexample.ui.base.activity.ActivityWrapper;

/**
 * отвечает за показывание и скрывание диалогов из презентера, когда вью является Activity
 */
@PerScreen
public class ActivityDialogManager implements DialogManager {
    private ActivityWrapper activityWrapper;

    @Inject
    public ActivityDialogManager(ActivityWrapper activityWrapper) {
        this.activityWrapper = activityWrapper;
    }

    @Override
    public void show(BaseDialog dialog) {
        dialog.show((AppCompatActivity) activityWrapper.get());
    }
}
