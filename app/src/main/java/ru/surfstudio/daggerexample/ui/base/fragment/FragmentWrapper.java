package ru.surfstudio.daggerexample.ui.base.fragment;


import android.support.v4.app.Fragment;

import javax.inject.Provider;

/**
 *
 */
public class FragmentWrapper {
    private Provider<Fragment> provider;

    public FragmentWrapper(Provider<Fragment> fragmentProvider) {
        this.provider = fragmentProvider;
    }

    public Fragment get(){
        return provider.get();
    }
}
