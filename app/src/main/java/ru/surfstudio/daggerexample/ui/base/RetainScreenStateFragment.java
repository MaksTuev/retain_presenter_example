package ru.surfstudio.daggerexample.ui.base;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import timber.log.Timber;

/**
 *
 */
public class RetainScreenStateFragment extends Fragment {

    private ScreenComponent screenComponent;
    private OnDestroyListener onDestroyListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Timber.d("AAA retain state fragment OnCreate");

        setRetainInstance(true);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Timber.d("AAA retain state fragment onDestroy");
        if(onDestroyListener!=null){
            onDestroyListener.onDestroy();
        }
    }

    public ScreenComponent getScreenComponent() {
        return screenComponent;
    }

    public void setScreenComponent(ScreenComponent screenComponent) {
        this.screenComponent = screenComponent;
    }

    public void setOnDestroyListener(OnDestroyListener onDestroyListener) {
        this.onDestroyListener = onDestroyListener;
    }

    public interface OnDestroyListener{
        void onDestroy();
    }
}
