package ru.surfstudio.daggerexample.ui.base.activity;

import android.support.v7.app.AppCompatActivity;

import ru.surfstudio.daggerexample.app.App;
import ru.surfstudio.daggerexample.app.AppComponent;

/**
 * бызовый класс всех Activity
 */
public abstract class BaseActivity extends AppCompatActivity{

    protected AppComponent getApplicationComponent() {
        return ((App) getApplication()).getAppComponent();
    }
}
