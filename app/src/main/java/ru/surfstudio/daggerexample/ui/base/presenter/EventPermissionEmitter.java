package ru.surfstudio.daggerexample.ui.base.presenter;

import ru.surfstudio.daggerexample.util.rx.SimpleOnSubscribe;
import rx.Subscriber;

/**
 *
 */
public class EventPermissionEmitter extends SimpleOnSubscribe<Void> {
    private boolean permissionGranted;
    private int unresolvedPermissionsCounter = 0;

    public void setPermissionGranted(boolean permissionGranted) {
        this.permissionGranted = permissionGranted;
        if(permissionGranted) {
            while (unresolvedPermissionsCounter != 0) {
                emit(null);
                unresolvedPermissionsCounter--;
            }
        }
    }

    @Override
    public void call(Subscriber<? super Void> subscriber) {
        super.call(subscriber);
    }

    public void requestPermission() {
        if(permissionGranted){
            emit(null);
        } else {
            unresolvedPermissionsCounter++;
        }
    }
}
