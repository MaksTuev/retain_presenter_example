package ru.surfstudio.daggerexample.interactor.bookfull;


import ru.surfstudio.daggerexample.entity.Book;
import ru.surfstudio.daggerexample.entity.bookstate.DownloadBookState;
import ru.surfstudio.daggerexample.entity.bookstate.PlayerBookState;

/**
 * вторичная модель, содержащая всю информацию о книге для отображении на UI
 */
public class BookFull {

    private Book book;

    private DownloadBookState downloadBookState;

    private PlayerBookState playerBookState;


    public DownloadBookState getDownloadBookState() {
        return downloadBookState;
    }

    public void setDownloadBookState(DownloadBookState downloadBookState) {
        this.downloadBookState = downloadBookState;
    }

    public PlayerBookState getPlayerBookState() {
        return playerBookState;
    }

}
