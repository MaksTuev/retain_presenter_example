package ru.surfstudio.daggerexample.entity.booksource;

import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import java8.util.stream.StreamSupport;
import rx.Observable;

public class ServerBookSources {

    /**
     * определяет, какие группы источников поддерживаются приложением и их приоритет
     */
    public static List<String> supportedGroups = Arrays.asList(
            ServerChapterSource.GROUP_STANDARD_128_KBPS,
            ServerChapterSource.GROUP_STANDARD_192_KBPS,
            ServerChapterSource.GROUP_STANDARD_64_KBPS,
            ServerChapterSource.GROUP_MOBILE_32_KBPS,
            ServerChapterSource.GROUP_MOBILE_16_KBPS,
            ServerChapterSource.GROUP_UNCKNOWN);

    private ArrayList<ServerChapterSource> chapterSources;

    public ServerBookSources(ArrayList<ServerChapterSource> serverChapterSources) {
        this.chapterSources = serverChapterSources;
    }

    @Nullable
    public ServerChapterSource getTrial() {
        return StreamSupport.stream(chapterSources)
                .filter(chapterSource -> chapterSource.getChapter() == ServerChapterSource.CHAPTER_TRIAL)
                .reduce(null, (result, element) -> element); //for return null, if not exist
    }

    /**
     * @param chapter
     * @return источник главы книги с наиболее приемлемым качеством
     */
    public ServerChapterSource getChapter(int chapter) {
        return Observable.merge(
                Observable.from(supportedGroups)
                        .flatMap(group -> Observable.from(chapterSources) //группируем по GroupId
                                .filter(chapterSource -> chapterSource.getGroupId().equals(group))
                                .toList())
                        .flatMap(groupedChapterSources -> Observable.from(groupedChapterSources))
                        .filter(chapterSource -> chapterSource.getChapter() == chapter), //выбираем нужные главы
                Observable.just(null)) //null значение если не найдем нужную главу
                .toBlocking()
                .first(); //берем только первую в списке выбранных
    }

    public int getNumChapters() {
        return Observable.from(supportedGroups)
                .flatMap(group -> Observable.from(chapterSources) //группируем по GroupId
                        .filter(chapterSource -> chapterSource.getGroupId().equals(group))
                        .toList())
                .map(groupedChapterSources -> groupedChapterSources.size()) //берем размеры групп
                .filter(numChapters -> numChapters != 0) //отсеиваем пустые
                .toBlocking()
                .first(); //берем только первое значение в списке выбранных
    }

    public ArrayList<ServerChapterSource> getChapterSources() {
        return new ArrayList<>(chapterSources);
    }
}
