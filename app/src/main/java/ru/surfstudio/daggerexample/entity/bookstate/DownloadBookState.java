package ru.surfstudio.daggerexample.entity.bookstate;

/**
 * состояние загрузки книги
 */
public class DownloadBookState {
    public static final int MAX_PROGRESS = 100;

    private String bookId;

    private DownloadStatus status;
    /**
     * пргресс в процентах
     */
    int progress;

    public DownloadBookState(String bookId, DownloadStatus status, int progress) {
        this.bookId = bookId;
        this.status = status;
        this.progress = progress;
    }

    public String getBookId() {
        return bookId;
    }

    public DownloadStatus getStatus() {
        return status;
    }

    public int getProgress() {
        return progress;
    }

    /**
     * @return загружается ли сейчас книга (или стоит в очереди на загрузку)
     */
    public boolean isStarted() {
        return status != DownloadStatus.NOT_STARTED
                && status != DownloadStatus.STOP
                && status != DownloadStatus.COMPLETE;
    }

    @Override
    public String toString() {
        return "DownloadBookState{" +
                "bookId='" + bookId + '\'' +
                ", status=" + status +
                ", progress=" + progress +
                '}';
    }

    public static DownloadBookState empty(String bookId) {
        return new DownloadBookState(bookId, DownloadStatus.NOT_STARTED, 0);
    }
}
