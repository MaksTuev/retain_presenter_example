package ru.surfstudio.daggerexample.entity;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import ru.surfstudio.daggerexample.entity.booksource.LocalBookSources;
import ru.surfstudio.daggerexample.entity.booksource.ServerBookSources;

/**
 * модель книги
 */
public class Book {

    private String id;

    private String uuid;

    private LocalBookSources localBookSources;

    private ServerBookSources serverBookSources;

    private String title;

    private ArrayList<Author> authors = new ArrayList<>();

    private String coverPreviewUrl;

    private boolean myBook;

    private boolean hidden; //equals archived

    public boolean isMy(){
        return myBook;
    }

    public boolean isHidden(){
        return hidden;
    }


    public String getId() {
        return id;
    }

    public String getUUID() {
        return uuid;
    }


    public Book(String title) {
        this.title = title;
    }

    @NonNull
    public LocalBookSources getLocalBookSources() {
        if(localBookSources == null){
            localBookSources = new LocalBookSources(new ArrayList<>());
        }
        return localBookSources;
    }

    public ServerBookSources getServerBookSources() {
        return serverBookSources;
    }

    public String getTitle() {
        return title;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public String getCoverPreviewUrl() {
        return coverPreviewUrl;
    }
}
