package ru.surfstudio.daggerexample.app;

import android.content.Context;

import dagger.Component;
import ru.surfstudio.daggerexample.interactor.bookfull.BookFullInteractor;
import ru.surfstudio.daggerexample.module.SessionStorage;
import ru.surfstudio.daggerexample.module.player.BookAudioPlayer;
import ru.surfstudio.daggerexample.module.player.PlayerBookStateProvider;
import ru.surfstudio.daggerexample.module.player.core.AudioPlayerCore;
import ru.surfstudio.daggerexample.module.player.storage.StaticPlayerBookStateRepository;
import ru.surfstudio.daggerexample.module.repository.BookRepository;

@Component(modules = AppModule.class)
@PerApplication
public interface AppComponent {

    //пробрасываем объекты из скоупа PerApplication в скоуп PerActivity
    Context context();
    SessionStorage sessionStorage();

    BookAudioPlayer bookAudioPlayer();
    PlayerBookStateProvider playerBookStateProvider();
    AudioPlayerCore audioPlayerCore();
    StaticPlayerBookStateRepository staticPlayerBookStateRepository();

    BookRepository bookRepository();

    BookFullInteractor bookFullInteractor();
}
