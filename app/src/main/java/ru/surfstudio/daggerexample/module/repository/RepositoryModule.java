package ru.surfstudio.daggerexample.module.repository;

import dagger.Module;
import dagger.Provides;
import ru.surfstudio.daggerexample.app.PerApplication;

@Module
public class RepositoryModule {

    @Provides
    @PerApplication
    BookRepository provideBookRepository(BookRepositoryImpl bookRepository){ //пример инъекции зависимостей с интерфейсами
        return bookRepository;
    }
}
