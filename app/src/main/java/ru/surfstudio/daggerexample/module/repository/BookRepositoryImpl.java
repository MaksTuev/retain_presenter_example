package ru.surfstudio.daggerexample.module.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import ru.surfstudio.daggerexample.app.PerApplication;
import ru.surfstudio.daggerexample.entity.Book;
import ru.surfstudio.daggerexample.module.SessionStorage;
import rx.Observable;

@PerApplication
public class BookRepositoryImpl implements BookRepository {

    private SessionStorage sessionStorage;

    @Inject
    public BookRepositoryImpl(SessionStorage sessionStorage) {
        this.sessionStorage = sessionStorage;
    }

    public Observable<Book> observeMyBookChanged() {
        return Observable.empty();
    }

    @Override
    public Observable<List<Book>> getBooks(int offset) {
        return Observable.timer(1000, TimeUnit.MILLISECONDS)
                .flatMap(t -> {
                            List<Book> result = new ArrayList<>();
                            for (int i = 0; i < 10; i++) {
                                result.add(new Book("Book " + (offset + i)));
                            }
                            return Observable.just(result);
                        });
    }
}
