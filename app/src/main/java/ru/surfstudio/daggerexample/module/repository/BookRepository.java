package ru.surfstudio.daggerexample.module.repository;

import java.util.List;

import ru.surfstudio.daggerexample.entity.Book;
import rx.Observable;

/**
 * Интерфейс репозитория с книгами
 */
public interface BookRepository {

    Observable<Book> observeMyBookChanged();

    Observable<List<Book>> getBooks(int offset);
}
