package ru.surfstudio.daggerexample.module.player.storage;

import com.j256.ormlite.field.DatabaseField;

import ru.surfstudio.daggerexample.entity.bookstate.PlayerBookState;
import ru.surfstudio.daggerexample.entity.bookstate.PlayingStatus;


/**
 * модель для хранения информации о состоянии книги в плеере между перезапусками приложения
 * Использовать эту модель можно только для получения из нее {@link PlayerBookState}
 */
//todo db model
public class StaticPlayerBookState {

    @DatabaseField(id = true)
    private String bookId;

    private int listenPosition = 0;

    private int listenChapter = 0;

    private double speedRatio = 1;

    public PlayerBookState transform(PlayingStatus currentPlayingStatus){
        return new PlayerBookState(bookId, listenChapter, listenPosition, speedRatio, currentPlayingStatus);
    }
}
